'''
@author: John Payne
'''

call_list = [10,20,34,21,15,6]

dis_array = \
    [
        [1.1,1.9,2.8,2.4,2.8,3.4],  # distance between potential location "1" and the neighborhood areas.
        [1.4,1.2,1.8,2.6,2.5,2.8],  # potential location "2"
        [3.6,2.4,1.3,3.7,2.6,1.6],  # potential location "3"
        [2.3,2.7,3.4,1.2,1.6,2.6],  # potential location "4"
        [3.4,2.6,2.2,2.7,1.8,1]     # potential location "5"
    ]

def fire_distance(index, candidate_dist, calls):
    total_dist = 0
    area_count = len(calls)

    for i in range(area_count):
        total_dist += calls[i] * candidate_dist[index][i]

    return total_dist

fire_candidates = len(dis_array)

distance_calc = []

for i in range(fire_candidates):
    dist_sum = fire_distance(i,dis_array, call_list)
    distance_calc.append(dist_sum)
print distance_calc

short_dist = min(distance_calc)
print "The shortest total projected distance traveled is " + str(short_dist)

best_candidate = distance_calc.index(short_dist)
print "The best candidate for the new fire station is location " + str(best_candidate+1)
