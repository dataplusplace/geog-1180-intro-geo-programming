def convertgrade(name,grade):
    if 94 <= grade <= 100:
        return(name+ "'s grade is: A")
    elif 90 <= grade <= 93:
        return(name+ "'s grade is: A-")
    elif 87 <= grade <= 89:
        return(name+ "'s grade is: B+")
    elif 84 <= grade <= 86:
        return(name+ "'s grade is: B")
    elif 80 <= grade <= 83:
        return(name+ "'s grade is: B-")
    elif 78 <= grade <= 79:
        return(name+ "'s grade is: C+")
    elif 75 <= grade <= 77:
        return(name+ "'s grade is: C")
    elif 70 <= grade <= 74:
        return(name+ "'s grade is: C-")
    elif 65 <= grade <= 69:
        return(name+ "'s grade is: D+")
    elif 60 <= grade <= 64:
        return(name+ "'s grade is: D")
    elif grade < 60:
        return(name+ "'s grade is: E")

def startgrade():
    innm = raw_input("Enter first name.\n")

    if innm.isalpha() == False:
        print "Please use only alphabetical characters."
        startgrade()

    ingrd = float(raw_input("Enter numeric score between 0 and 100.\n"))

    if ingrd > 100.0 or ingrd < 0.0:
        print "Score is out of range."
        startgrade()

    print convertgrade(innm,int(round(ingrd,0)))

startgrade()