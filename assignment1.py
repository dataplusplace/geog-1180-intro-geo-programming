from math import* 

x1=422504.192
y1=4498778.552
x2=271162.223
y2=4252186.393
M_dis=abs(x1-x2)+abs(y1-y2) 
E_dis=sqrt((x1-x2)**2+(y1-y2)**2) 
print "Manhattan distance between Salty Lake City and Beaver County is %f" %M_dis
print "Euclidean distance between Salty Lake City and Beaver County is %f" %E_dis
