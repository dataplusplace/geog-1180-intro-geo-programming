'''
@author: John Payne
'''
# grade format(name,grade1,grade2,grade3)
grade='Alice,68,70,90;Chris,95,90,87;Cindy,89,80,75;Adam,75,70,69;Richard,81,80, 93;Ryan,51,62,70;Edward,88,90,94;Larry,93,95,97;Seth,67,70,80'

semi_split = grade.split(';')

stdnt_list = []

for student in semi_split:
    comma_split = student.split(',')
    stdnt_list.append(comma_split)

# print stdnt_list

stdnt_dict = {}

for student in stdnt_list:
    stdnt_dict[student[0]] = student[1:]

print stdnt_dict

avg_grade = {}

for student, grd_list in stdnt_dict.items():
    # avg_grade.key = (sum(grd_list[:]) / len(grd_list[:])) # not sure how to get the list to int without googlefu shenanigans
    avg_grade[student] = ((int(grd_list[0]) + int(grd_list[1]) + int(grd_list[2])) / 3)

# print sorted(avg_grade)

for key, value in sorted(avg_grade.iteritems(), key=lambda (k,v): (v,k)): # Hooray, googlefu shenanigans after all!
    print "%s: %s" % (key, value)
