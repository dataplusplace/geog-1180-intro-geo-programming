'''
@author: John Payne
'''

from math import sqrt

k = 1
a = 2
psubi = 4089
shr_psubj = 4.9
mir_psubj = 4.7
shr_d = 4.2
mir_d = 5.8
d_ab = 4.3

shr_i = (k*psubi*shr_psubj)/shr_d**a

print "Gravity model interaction score between Sugar House and Shriner's Hospital for Children is " + str(shr_i)

mir_i = (k*psubi*mir_psubj)/mir_d**a

print "Gravity model interaction score between Sugar House and Children's Miracle Hospital is " + str(mir_i)

d_az = d_ab/(1+sqrt(mir_psubj/shr_psubj))

print "Breaking point between Shriner's and Children's Miracle hospitals is " + str(d_az) + " miles"
