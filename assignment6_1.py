'''
@author: John Payne
'''

# The format of the locations of these guys is [x,y]
locations = [[427016.87,4512645.10],[427233.87,4512762.30],[427785.73,4512812.08],[427464.42,4511229.45],[428449.54,4511194.56]]

xcoords = [i[0] for i in locations]
ycoords = [i[1] for i in locations]

meanx = sum(xcoords)/len(xcoords)
meany = sum(ycoords)/len(ycoords)

print 'The mean center of the locations is at coordinate (' + str(meanx) + ',' + str(meany) + ')'