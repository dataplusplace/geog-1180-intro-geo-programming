'''
@author: John Payne
'''
from math import sqrt
from math import atan
from math import degrees

# data format [x,y,elevation] in meter
top = [445373.59,4490238.66,3339]
bottom = [444445.75,4492462.02,2472]

topx = top[0]
topy = top[1]
topz = top[2]

botx = bottom[0]
boty = bottom[1]
botz = bottom[2]

slope_dist = sqrt( (topx - botx)**2 + (topy - boty)**2 )
slope_elev = topz - botz

slope = slope_elev / slope_dist

slope_degree = degrees(atan(slope))

print 'The degree of the slope is ' + str(slope_degree)
