'''
Created on Jul 24, 2014

@author: John Payne
'''
# the centroid coordinates (in meters) for each county in Utah
# format: county name, x coordinate,y coordinate
strCounties = 'SALT LAKE,421932.509151,4502229.11769;KANE,421248.077003,4126997.12719;MILLARD,318229.975165,4327038.17395;SANPETE,450311.582705,4358372.7655;CARBON,535286.548662,4388828.93126;UTAH,442922.507375,4441252.65588;CACHE,438164.934916,4619192.03201;SUMMIT,503745.916187,4524184.01594;WASHINGTON,277954.402951,4128934.44837;GRAND,623880.419923,4315675.1777;UINTAH,626223.592603,4442460.96986;TOOELE,319270.590251,4479721.51259;SEVIER,430056.793418,4289103.62028;GARFIELD,460935.241012,4189974.09249;SAN JUAN,605294.515606,4164841.18364;BOX ELDER,326211.275652,4598715.29279;IRON,298574.110588,4192702.51903;WEBER,423471.784425,4569140.26507;EMERY,525940.528322,4316385.26069;RICH,479645.42659,4608899.86246;WASATCH,485711.183527,4464442.98073;BEAVER,304642.75275,4247877.5251;DAGGETT,625788.907604,4527355.62639;DAVIS,406521.343767,4538251.09871;WAYNE,508428.89395,4241934.48456;PIUTE,401491.210781,4243766.28213;MORGAN,451835.27776,4548811.17842;DUCHESNE,548843.456439,4460927.19722;JUAB,347009.389764,4396492.15746'
# please start to write your code here

from math import sqrt

items = strCounties.split(';')
SaltLake = items[0]
# print SaltLake.split(',')
SaltLakesplit = SaltLake.split(',')
# print saltLakesplit

county_coords = []

for centroid in items:
    xy = centroid.split(',')
    if xy[0] != 'SALT LAKE':
        county_coords.append(xy)

coord_distance = []

for county in county_coords:
    distance = sqrt((float(SaltLakesplit[1]) - float(county[1]))**2 + (float(SaltLakesplit[2]) - float(county[2]))**2)
    print 'The distance between SALT LAKE and ' + str(county[0]) + ' Counties is ' + str(round(distance,0)) + ' meters' #%(j[0],dis)
    coord_distance.append([county[0],distance])

max_county = coord_distance[0][0]
min_county = coord_distance[0][0]
max_distance = coord_distance[0][1]
min_distance = coord_distance[0][1]

for county in coord_distance:
    if county[1] > max_distance:
        max_distance = county[1]
        max_county = county[0]
    if county[1] < min_distance:
        min_distance = county[1]
        min_county = county[0]

print 'Maximum distance is ' + str(round(max_distance)) + ' meters to ' + str(max_county) + ' County'
print 'Minimum distance is ' + str(round(min_distance)) + ' meters to ' + str(min_county) + ' County'

# Earlier attempts below
# items = strCounties.split(';')
# SaltLake = items[0]
# SaltLake = SaltLake.split(',')
#
# county = []
#
# for centroid in items:
#     centroid.split(',')
#     # county.append(centroid[0],float(centroid[1]),float(centroid[2]))
#     county.append(centroid)
#
# print county
#
# county.remove(county[0])
#
# def distance(point, c_point):
#         return point[0], sqrt((point[1]-c_point[1])**2+(point[2]-c_point[2])**2)
#
# # def distancelist(point, c_point):
# #         return point[0], sqrt((point[1]-c_point[1])**2+(point[2]-c_point[2])**2)
#
# def countycompare(county_dist):
#     for point in county:
#         county_dist.append(distance(point, SaltLake))
#     # print 'Nearby point IDs:', pt_list
#     # print 'Total nearby points:', len(pt_list)
#
# countycompare([])
